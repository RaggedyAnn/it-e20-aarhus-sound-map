//template used from mappa.js example

let myMap;
let canvas;
const mappa = new Mappa('Leaflet');
var sounds;
var locations;
var audioArray;
var loadAudio = 0;

function preload() {
  sounds = loadJSON("sounds.json");
  soundFormats('m4a', 'wav');
}

const options = {
  lat: 56.155,
  lng: 	10.201,
  zoom: 14,
  style: "http://{s}.tile.osm.org/{z}/{x}/{y}.png"
}

function setup(){
  canvas = createCanvas(windowWidth,windowHeight);
  audioArray = [];
  locations = sounds.locations;

  for (l of locations) {
    var s = loadSound('assets/' + l.sound, loaded);
    append(audioArray,s);
  }
  myMap = mappa.tileMap(options);
  myMap.overlay(canvas);
}

function loaded() {
  loadAudio++;
}

function draw(){
  clear();

  for(l of locations) {
    var pixel = myMap.latLngToPixel(l.long, l.lat);
    fill(255,0,0);
    noStroke();
    ellipse(pixel.x, pixel.y, 15, 15);
    if(dist(mouseX,mouseY,pixel.x,pixel.y) < 15) {
      fill(255,200);
      textSize(12);
      textStyle(BOLD);
      rect(pixel.x,pixel.y-12,textWidth(l.name),14);
      fill(0);
      text(l.name,pixel.x,pixel.y);
    }
  }
  if(loadAudio < locations.length) {
    fill(255);
    rect(0,0,width,height);
    fill(0);
    textSize(16);
    text(floor((loadAudio/locations.length)*100) + "%", width/2, height/2);
  }
}

function mousePressed() {
  if(loadAudio == audioArray.length) {
    for (j of audioArray) {
      if (j.isPlaying()) {
        j.jump();
        j.pause();
      }
    }

    for (var i = 0; i < locations.length; i++) {
      var pixel = myMap.latLngToPixel(locations[i].long, locations[i].lat);
      if(dist(mouseX,mouseY,pixel.x,pixel.y) < 15) {
        audioArray[i].play();
      }
    }
  }
}
